#!/usr/bin/python

import sys
import time

"""A spinner for displaying progress of output generating computations."""


class Spinner(object):
  _SPINNER = ('/', '-', '\\', '|')

  def __init__(self, wait_msg=None, done_msg=None, in_file=None, out_file=None,
               freq=0):
    self._wait_msg = '' if wait_msg is None else wait_msg + ' '
    self._done_msg = 'done' if done_msg is None else done_msg
    self._in_file = in_file or sys.stdin
    self._out_file = out_file or sys.stdout
    self._delay = 1.0 / (freq if freq > 0 else 25)
    self._idx = -1

  def _Print(self, trailer):
    self._out_file.write('\r%s%s' % (self._wait_msg, trailer))
    self._out_file.flush()

  def _PrintSpinner(self):
    self._idx = (self._idx + 1) % len(self._SPINNER)
    self._Print(self._SPINNER[self._idx])

  def _PrintDone(self):
    self._Print(self._done_msg + '\n')

  def Run(self):
    next_time = time.time()
    self._PrintSpinner()
    while self._in_file.readline():
      curr_time = time.time()
      if curr_time >= next_time:
        next_time = curr_time + self._delay
        self._PrintSpinner()

    self._PrintDone()


if __name__ == '__main__':
  wait_msg = sys.argv[1] if len(sys.argv) > 1 else None
  done_msg = sys.argv[2] if len(sys.argv) > 2 else None
  spinner = Spinner(wait_msg=wait_msg, done_msg=done_msg)
  spinner.Run()
